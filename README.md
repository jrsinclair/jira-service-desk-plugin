# Jira Service Desk Remote Interviewee Homework Task

Hi, my name’s James, and I’m applying for a role at Atlassian. But, I’m imagining you’ve set this task because you’ve got no idea whether I can code or not. Anyone can write buzzwords on their résumé. And anyone can call themselves a full-stack developer. But actually being able to write working code is another thing.

Perhaps even more importantly, can I communicate about my code? Some people can write code that works, but they don’t work well in a team. Being a senior developer involves making hard choices between different design trade-offs, and then explaining to other people why you think that’s a good idea. So, I’m guessing part of this exercise is to see if I can talk intelligently about why I’ve made the design choices I have.

So that brings us to the coding task. The brief was to create a Jira Service Desk add-on that will list the top reporters for the project. Other than that, the brief was pretty broad. It didn’t specify what fields to display or what users might want to link to.

## My Approach

To start with I ran through the *Getting Started* tutorial, and then the *Jira Service Desk* tutorial. That second tutorial set up a basic add-on framework, so I decided to re-use that for the task. The front-end code was using an old version of Jira stylesheets though and used jQuery to do DOM manipulation. So, I took all that out. I made the assumption that for a coding task like this, using modern JS would be fine. In practice, it looks like you guys use React, so I figure we'd be transpiling code anyway for older browsers.

At first glance, I couldn’t find anything in the API documentation that would do aggregation. This bothered me because I didn’t want to transfer large amounts of data to process on the client side. I even wrote a version of the add-on that used the JQL interface and counted up the results. But it didn't feel right. Eventually, I discovered the [Jira expressions API](https://developer.atlassian.com/cloud/jira/service-desk/jira-expressions/). And that did what I wanted. It allowed me to make a request then aggregate the results server-side using a `reduce()` function. 

The task stated that add-on needed to list users for the current project. The only way I could find to get information about the current project was through the `AP.context` API. So, since I was looking at two requests with callbacks, I decided to make promise wrappers so it would be a little easier to coordinate. They're not terribly complicated:

```javascript
// Promisified version of AP.context.getContext()
function pGetContext() {
    return (new Promise(resolve => AP.context.getContext(resolve)));
}

// Promisified version of AP.request()
function pRequest(opts) {
    return (new Promise(
        (resolve, reject) => AP.request({...opts, success: resolve, error: reject}))
    );
}
```

So first we grab the context. Then we extract the project key. And then build the actual Jira Expression request. The request looks something like this:

```javascript
// Use JIRA expression API to request all service requests for the given project
// and then count them by user.
function requestReporterCounts(project) {
    return pRequest({
        url: '/rest/api/2/expression/eval',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            expression: `issues.reduce(
                (acc, issue) => acc.set(
                    issue.reporter.accountId,
                    {
                        reporter: issue.reporter,
                        count:    ((acc[issue.reporter.accountId] || {}).count || 0) + 1
                    }
                ),
                new Map()
            )`,
            context: {
                issues: {
                    jql: {
                        query: interpolate(SEARCHJQL)({project}),
                        startAt: 0,
                        // TODO: Limit the number of responses to a sensible amount.
                    }
                }
            }
        })
    });
}
```

Once the data comes back, I run it through a pipeline and insert it into the DOM. That uses a `pipe()` function. It's a utitlity for composing a bunch of smaller utility functions together. The pipeline looks like this:

```javascript
const displayReporters = pipe(
    JSON.parse,
    prop('value'),
    toValues,
    sort(compareReporters),
    map(pipe(liftReporter, liftAvatar, reporterToRow)), // Use pipe here so we don't traverse the array three times.
    join('\n'),
    ifElse(isEmpty)(emptyMsg)(buildTable),
    appendTo(document.querySelector('#main-content')),
);
```

With those in place, we end up with a neat promise chain like this:

```javascript
pGetContext()
    .then(getProjectKey)
    .then(requestReporterCounts)
    .then(displayReporters)
    .catch(displayError);
```

## Things I didn't do

Now, as you can see, it’s not really doing much other than list the sorted data in a table. I didn’t push too much further into this as the brief was to “treat is as very rough proof of concept”. So there are a number of things I *didn’t* do:

1. **I didn’t set up React or use the Atlaskit design components (other than the basic CSS)**. The tutorials didn’t cover this, and it seemed like it would be a distraction. I would imagine that in a real-world scenario I would be working on an existing codebase with this already configured, or I would be able to figure that out as needed.
2. **I didn’t add any links from the table anywhere else.** I would imagine that if this was a real plugin, you’d want to have it do more than just list the reporters. You’d want to links to perhaps list all the issues that this reporter had created. That would have required another request to get the base URL (as far as I could tell) though. That would have complicated the task somewhat.
3. **I didn't check this in IE or Edge** as again, I figured that a real production application would be transpiling JSX. As such issues with ES6 syntax would be very different.
4. **I didn't write unit tests or end-to-end tests**. Normally I would prefer to write tests first, but I didn't want to add a lot of noise to the repository. If you're interested in seeing how I write tests, or my thinking about that, I've written several blog posts on the topic.

But, as you can see, the code is working and fulfils the basic requirements. I’m happy to discuss anything further if necessary.