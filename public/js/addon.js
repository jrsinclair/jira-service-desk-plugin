/**
 *  Top Reporters frontend script
 * 
 * @author James Sinclair <jrsinclair@gmail.com>
 * 
 * May 2019
 */
/* global document, AP */
const SEARCHJQL = 'project = {{project}} AND issuetype in ("Service Request", "Service Request with Approvals")';

// I'm assuming we can use ES6 here… In a production app, I'd imagine we'd be
// transpiling JSX. Most functions could be rewritten using ES5 syntax if
// support for older browsers was an issue.

// General Helper Functions.
// ---------------------------------------------------------------------------------
const prop     = k => x => x[k];
const pipe     = (...fns) => (...args) => fns.reduce((res, fn) => [fn.call(null, ...res)], args)[0];
const map      = fn => a => a.map(fn);
const toValues = o => Object.values(o);
const join     = glue => a => a.join(glue);
const appendTo = el => x => {el.innerHTML += x; return el;};
const ifElse   = testFn => thenFn => elseFn => x => (testFn(x)) ? thenFn(x) : elseFn(x);
const isEmpty  = x => (x.length === 0);
const sort     = compareFn => a => {
    const tmp = a.slice(0); // Take a copy of the array so as not to modifiy the original.
    return tmp.sort(compareFn);
};
const interpolate = tpl => data => Object.entries(data).reduce(
    (str, [key, val]) => str.replace(new RegExp(`{{${key}}}`, 'g'), val),
    tpl
);

// Functions specific to Top Reporters
// ---------------------------------------------------------------------------------

// Promisified version of AP.context.getContext()
function pGetContext() {
    return (new Promise(resolve => AP.context.getContext(resolve)));
}

// Promisified version of AP.request()
function pRequest(opts) {
    return (new Promise(
        (resolve, reject) => AP.request({...opts, success: resolve, error: reject}))
    );
}

const getProjectKey    = x => x.jira.project.key;
const liftAvatar       = x => ({...x, avatar: x.avatarUrls['48x48']});
const liftReporter     = x => ({...x.reporter, count: x.count});
const compareReporters = (a, b) => (b.count - a.count);

const reporterToRow = interpolate(`<tr>
    <td><img src="{{avatar}}" alt="" width="48" height="48"/></td>
    <td>{{displayName}}</td>
    <td>{{count}}</td>
</tr>`);

function buildTable(rowData) {
    return `<table class="aui top-reporters__table">
  <thead>
      <th></th>
      <th>Name</th>
      <th>Requests</th>
  <thead>
  <tbody>
      ${rowData}
  </tbody>
</table>`;
}

function emptyMsg() {
    return `<div class="message">
        <span class="ak-icon ak-icon__size-large info__icon">ℹ</span>
        <p>No one has reported any issues yet.</p>
    </div>`;
}

function renderError({status, statusText}) {
    return `<div class="error">
        <span class="ak-icon ak-icon__size-large error__icon">&times;</span>
        <p class="error__title"><strong>An error occurred:</strong></p>
        <p class="error__details">${status}: ${statusText}</p>
    </div>`;
}

// Given a response back from the API, transform it into an HTML string and
// inject it into the DOM.
const displayReporters = pipe(
    JSON.parse,
    prop('value'),
    toValues,
    sort(compareReporters),
    map(pipe(liftReporter, liftAvatar, reporterToRow)), // Use pipe here so we don't traverse the array three times.
    join('\n'),
    ifElse(isEmpty)(emptyMsg)(buildTable),
    appendTo(document.querySelector('#main-content')),
);

// Given an error response back from the API, turn it into a suitable HTML
// string and inject it into the DOM
const displayError = pipe(
    renderError,
    appendTo(document.querySelector('#main-content')),
);

// Use JIRA expression API to request all service requests for the given project
// and then count them by user.
function requestReporterCounts(project) {
    return pRequest({
        url: '/rest/api/2/expression/eval',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            expression: `issues.reduce(
                (acc, issue) => acc.set(
                    issue.reporter.accountId,
                    {
                        reporter: issue.reporter,
                        count:    ((acc[issue.reporter.accountId] || {}).count || 0) + 1
                    }
                ),
                new Map()
            )`,
            context: {
                issues: {
                    jql: {
                        query: interpolate(SEARCHJQL)({project}),
                        startAt: 0,
                        // TODO: Limit the number of responses to a sensible amount.
                    }
                }
            }
        })
    });
}

// Main thread.
// ---------------------------------------------------------------------------------
pGetContext()
    .then(getProjectKey)
    .then(requestReporterCounts)
    .then(displayReporters)
    .catch(displayError);
